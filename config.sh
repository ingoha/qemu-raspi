#ARCH=armhf
ARCH=arm64
# FIXME Image-Name aus URL parsen...
RASPIOS_DATESTAMP="2024-07-04"
RASPIOS_CODENAME="bookworm"
RASPIOS_IMAGE_NAME="${RASPIOS_DATESTAMP}-raspios-${RASPIOS_CODENAME}-${ARCH}-lite"
# https://www.raspberrypi.com/software/operating-systems/
RASPIOS_IMAGE_URL="https://downloads.raspberrypi.org/raspios_lite_${ARCH}/images/raspios_lite_${ARCH}-${RASPIOS_DATESTAMP}/${RASPIOS_IMAGE_NAME}.img.xz"
KERNEL="kernel8.img"
DTB="bcm2710-rpi-3-b-plus.dtb"
KERNEL_NAME="${RASPIOS_DATESTAMP}-kernel8.img"
DTB_NAME="${RASPIOS_DATESTAMP}-bcm2710-rpi-3-b-plus.dtb"
ENABLE_SSH=true

# cf. https://github.com/berdav/qemu-rpi-gpio/blob/master/qemu-pi-setup/run.sh
# Enable or disable GPIO management
#ENABLEQTEST=true
ENABLEQTEST=false
QTESTSOCKET="localhost:2223"
QTESTPARAMS=""
if $ENABLEQTEST; then
	QTESTPARAMS="$QTESTPARAMS -qtest telnet:$QTESTSOCKET"
fi
