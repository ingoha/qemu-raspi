#!/bin/bash
. ./config.sh
# cf. https://github.com/dhruvvyas90/qemu-rpi-kernel/tree/master/native-emulation
sudo qemu-system-aarch64 -M raspi3b \
-append "rw earlyprintk loglevel=8 dwc_otg.lpm_enable=0 root=/dev/mmcblk0p2 rootdelay=1" \
-dtb $DTB_NAME \
-drive file=$RASPIOS_IMAGE_NAME.img,if=sd,format=raw \
-kernel $KERNEL_NAME -m 1G -smp 4 \
-serial stdio -usb -device usb-mouse -device usb-kbd \
-device usb-net,netdev=net0 -netdev user,id=net0,hostfwd=tcp::2222-:22 \
$QTESTPARAMS
