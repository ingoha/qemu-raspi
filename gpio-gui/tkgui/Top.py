import random
from tkinter import *

class TopFrame(Frame):
    def __init__(self, parent):
        super().__init__(parent, bg="#AAAAAA")

        self.parent = parent
        self.startVar = False

        self.grid()
        
        self.gpios = []
        for nr in range(1,32):
            Label(self, text=str(nr), bg='#AAAAAA').grid(column=(nr - 1), row=0)
            label = StringVar()
            self.gpios.append(label)
            label.set("-")
            Label(self, textvariable=label, bg='#AAAAAA').grid(column=(nr-1), row=1)
            self.grid_columnconfigure((nr-1), minsize=40)
        self.startBt = Button(self, text="Start", command= lambda: self.start())
        self.startBt.grid(column=32, row=0, rowspan=2)

    def start(self):
        print("start")
        if self.startVar:
            self.startVar = False
        else:
            self.startVar = True

    def akt(self):
        if self.startVar:
            for nr in range(0,31):
                l = self.parent.manager.parse("get "+str(nr+1))
#                print("get "+str(nr+1))
                if l == "True":
                    self.gpios[nr].set("1")
                else:
                    self.gpios[nr].set("0")
