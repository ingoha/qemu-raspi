import random
import time
from tkinter import *
from tkinter import ttk
from tkinter import simpledialog
from PIL import Image, ImageTk

class InputFactory():
    def build(self, parent, name, nr, prell):
        switcher = {
            "taster": Taster(parent, nr, prell),
            "schalter": Schalter(parent, nr, prell)
        }
        return switcher.get(name, "schalter")

class Schalter(Frame):
    def __init__(self, parent, nr, prell):
        super().__init__(parent)

        self.parent = parent
        self.nr = nr
        self.prell = prell
        self.state = 0

#        self.grid()

        self.heading = Label(self, text="GPIO "+str(nr))
        self.heading.grid(column=0, row=0, columnspan=2)
        
        self.schalter = Button(self, text="S", command=self.toggle)
        self.schalter.grid(column=0, row=1)

        self.deleteBt = Button(self, text="-", command=lambda: delete(self))
        self.deleteBt.grid(column=1, row=1)

    def toggle(self):
        if self.state == 0:
            if self.prell:
                prellen(self, self.state)
            self.state = 1
        else:
            if self.prell:
                prellen(self, self.state)
            self.state = 0
        self.parent.parent.manager.parse("set "+str(self.nr)+" "+str(self.state))
        print("set "+str(self.nr)+" "+str(self.state))
    
class Taster(Frame):
    def __init__(self, parent, nr, prell):
        super().__init__(parent)

        self.parent = parent
        self.nr = nr
        self.prell = prell

#        self.grid()

        self.heading = Label(self, text="GPIO "+str(nr))
        self.heading.grid(column=0, row=0, columnspan=2)

        self.taster = Button(self, text="T")
        self.taster.grid(column=0, row=1)
        self.taster.bind("<ButtonPress>", self.on_press)
        self.taster.bind("<ButtonRelease>", self.on_release)

        self.deleteBt = Button(self, text="-", command=lambda: delete(self))
        self.deleteBt.grid(column=1, row=1)

    def on_press(self, event):
        self.parent.parent.manager.parse("set "+str(self.nr)+" 1")
        self.taster.configure(text="on")
        if self.prell:
            prellen(self, 1)
        else:
            print("set "+str(self.nr)+" 1")

    def on_release(self, event):
        self.parent.parent.manager.parse("set "+str(self.nr)+" 0")
        self.taster.configure(text="off")
        if self.prell:
            prellen(self, 0)
        else:
            print("set "+str(self.nr)+" 0")


class InputDialog(simpledialog.Dialog):
    ok = False

    def __init__(self, parent):
        super().__init__(parent, "Input Dialog")
        
    def body(self, frame):
        self.nrLabel = Label(frame, text="GPIO")
        self.nrVar = StringVar(frame)
        self.nrVar.set("1")
        self.nrEntry = Entry(frame, textvariable=self.nrVar)
        self.nrLabel.grid(column=0, row=0)
        self.nrEntry.grid(column=1, row=0)

        self.nameLabel = Label(frame, text="Input Art")
        self.nameVar = StringVar(frame)
        self.nameOptions = ["taster", "schalter"]
        self.nameVar.set(self.nameOptions[0])
        self.nameEntry = OptionMenu(frame, self.nameVar, *self.nameOptions)
        self.nameLabel.grid(column=0, row=1)
        self.nameEntry.grid(column=1, row=1)

        self.prellLabel = Label(frame, text="Prellen?")
        self.prellVar = IntVar(frame)
        self.prellButton = Checkbutton(frame, variable=self.prellVar, onvalue=True, offvalue=False)
        self.prellLabel.grid(column=0, row=2)
        self.prellButton.grid(column=1, row=2)

        self.bts = Frame(frame)
        self.bts.grid(column=1, row=3)

        self.ok_button = Button(self.bts, text='OK', command=self.ok_pressed)
        self.ok_button.grid(column=0, row=0, padx=10, pady=10)
        cancel_button = Button(self.bts, text='Cancel', command=self.cancel_pressed)
        cancel_button.grid(column=1, row=0, padx=10, pady=10)

    def buttonbox(self):
        return None

    def ok_pressed(self):
        self.ok = True
        self.destroy()

    def cancel_pressed(self):
        self.ok = False
        self.destroy()


def prellen(self, state):
        azList = [3,5,7]
        az = random.choice(azList)
        tms = random.choices(range(50,150), k = az)
        for t in tms:
            self.parent.parent.manager.parse("set "+str(self.nr)+" "+str(state))
            print("set "+str(self.nr)+" "+str(state))
            if state == 0:
                state = 1
            else:
                state = 0
            time.sleep(t/1000.)

def delete(self):
    self.grid_remove()
    self.parent.removeInP(self)