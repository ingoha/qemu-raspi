# Installation (Linux)
Am Beispiel der ET-VM (also LinuxMint 20.3 bzw. Ubuntu 20.04)
## QEMU
Es wird eine aktuelle Version (mind. 5) benötigt, die in den offiziellen Paketquellen nicht verfügbar ist. 

### Paketquelle (PPA)
Daher wird hierfür ein [PPA](https://launchpad.net/~canonical-server/+archive/ubuntu/server-backports) verwendet:

```
sudo apt-add-repository ppa:canonical-server/server-backports
sudo apt update
```

### Installation der Pakete
```
sudo apt -y install qemu-utils qemu-system-arm
```

## Python-Pakete
Für die Oberfläche wird ein Python-Paket benötigt:

```
sudo apt-get install python3-pil.imagetk
```

## Virtuelle SD-Karte
Mit dem Befehl ```./setup.sh``` wird das Image heruntergeladen und der QEMU-Pi für den ersten Start konfiguriert.

# Erster Start
Der QEMU-Pi kann entweder mit oder ohne GPIO-Simulation gestartet werden. In der Ausgangskonfiguration ist sie deaktiviert.

Mit dem Befehl ```./start.sh``` wird der QEMU-Pi gestartet. **Wichtig**: Das Terminal ist mit der UART-Schnittstelle verbunden; wird es geschlossen oder per Strg-C unterbrochen, wird der Pi sofort gestoppt.

In einem anderen Fenster kann man sich nach dem Ende des Startvorgangs (kann länger dauern) per SSH auf den QEMU-Pi verbinden, am einfachsten geht das per ```./ssh.sh```.

# Einrichtung im QEMU-Pi
Wie ein richtiger RaspberryPi sind nach dem ersten Start einige Schritte auszuführen, damit das System optimal funktioniert:

## Größe der SD-Karte anpassen
TBD, s.a. setup.sh

```sudo raspi-config```

# Benutzung der GPIO-Simulation
Der GPIO-Chip wird immer simuliert, ist also vom ersten Start an benutzbar. 

Sollen LEDs bzw. Taster/Schalter für den Benutzer sichtbar simuliert werden, muß zusätzlich zum QEMU-Pi noch die dazugehörige Oberfläche gestartet werden.

**Wichtig**: Die Oberfläche (Python-Skript) muß immer vor dem QEMU-Pi gestartet werden. Weiterhin muß in der Datei ```config.sh``` der Parameter ```ENABLEQTEST``` auf ```true``` gesetzt werden.



# Links/Quellen
* [Ergebnisse der Studienarbeit zum Thema](https://gitlab.com/Cadamir/qemu_raspi_gpio)
* https://projects-raspberry.com/simulation-raspberry-pi-emulator-for-windows-10-using-qemu/
* https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-05-28/2021-05-07-raspios-buster-armhf-lite.zip
* https://yushulx.medium.com/how-to-resize-raspbian-image-for-qemu-on-windows-ac0b44075d8f
* https://tinyapps.org/docs/mount_partitions_from_disk_images.html
 