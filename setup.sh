#!/bin/bash
. ./config.sh
# FIXME check if (extracted) image already exists
wget -c $RASPIOS_IMAGE_URL
#unzip $RASPIOS_IMAGE_NAME.zip
echo "-> unpacking image"
xz -d $RASPIOS_IMAGE_NAME.img.xz
echo "-> resizing image to 8G"
qemu-img resize $RASPIOS_IMAGE_NAME.img 8G
# DTB und Kernel aus Image extrahieren...
# cf. https://stackoverflow.com/questions/61562014/qemu-kernel-for-raspberry-pi-3-with-networking-and-virtio-support
./extract.sh
# userconf (cf. https://www.raspberrypi.com/documentation/computers/configuration.html#configuring-a-user)
# mount bootfs
echo "-> userconf & ssh"
sudo losetup -o $[ 8192 * 512 ] /dev/loop99 $RASPIOS_IMAGE_NAME.img
sudo mount /dev/loop99 /mnt
echo -n "pi:" | sudo tee /mnt/userconf.txt
openssl passwd -6 raspberry | sudo tee -a /mnt/userconf.txt
# SSH
if $ENABLE_SSH; then
        sudo touch /mnt/ssh.txt
fi
#unmount bootfs
sudo umount /dev/loop99
sudo losetup -d /dev/loop99
# im Pi: resize, Kernel-Version pinnen
#   aufräumen; bluetooth aus/weg, ...
#	sudo systemctl disable rpi-eeprom-update hciuart
#	raspi-config: ssh, ...
#	SWAP?!?
#		cf. https://www.elektronik-kompendium.de/sites/raspberry-pi/2002131.htm
