#!/bin/bash
. ./config.sh
echo "-> extracting kernel and dtb"
sudo losetup -o $[ 8192 * 512 ] /dev/loop99 $RASPIOS_IMAGE_NAME.img
sudo mount /dev/loop99 /mnt
cp /mnt/$KERNEL ./$KERNEL_NAME
cp /mnt/$DTB ./$DTB_NAME
sudo umount /dev/loop99
sudo losetup -d /dev/loop99
