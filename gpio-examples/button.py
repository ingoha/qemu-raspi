import gpiod

BUTTONPIN=2

chip=gpiod.Chip('gpiochip0')

button=chip.get_line(BUTTONPIN)
button.request(consumer='button', type=gpiod.LINE_REQ_DIR_IN)

print("Waiting for button")

while(button.get_value()==0):
  pass

print("done.")

button.release()