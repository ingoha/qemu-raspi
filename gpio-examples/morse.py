import gpiod

BUTTONPIN=2
LEDPIN=17

chip=gpiod.Chip('gpiochip0')

button=chip.get_line(BUTTONPIN)
button.request(consumer='button', type=gpiod.LINE_REQ_DIR_IN)
led=chip.get_line(LEDPIN)
led.request(consumer='blink', type=gpiod.LINE_REQ_DIR_OUT, default_val=0)

while(True):
  led.set_value(not button.get_value())