import gpiod
import time

LEDPIN = 17
INTERVAL_SEC = 1

chip=gpiod.Chip('gpiochip0')

led=chip.get_line(LEDPIN)
led.request(consumer='blink', type=gpiod.LINE_REQ_DIR_OUT, default_val=0)

while True:
    led.set_value(1)
    time.sleep(INTERVAL_SEC)
    led.set_value(0)
    time.sleep(INTERVAL_SEC)